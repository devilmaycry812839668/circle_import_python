from typing import TYPE_CHECKING


if TYPE_CHECKING:
    import x_class


class YClass:
    def f(self, x: "x_class.XClass"):
        print(x.value)
