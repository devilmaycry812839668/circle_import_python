# from y_class import YClass
# from y_class_1 import YClass
# from y_class_2 import YClass
from y_class_3 import YClass


class XClass:
    def __init__(self, value: int, y: YClass):
        self.value = value
        y.f(self)


if __name__ == '__main__':
    print("hello, main running, in")
    XClass(value=3, y=YClass())
    print("hello, main running, out")
